package com.service.javabitbuc.controller;


import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.apache.servicecomb.provider.rest.common.RestSchema;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.CseSpringDemoCodegen", date = "2018-04-27T15:48:18.419Z")

@RestSchema(schemaId = "javabitbuc")
@RequestMapping(path = "/javabitbuc", produces = MediaType.APPLICATION_JSON)
public class JavabitbucImpl {

    @Autowired
    private JavabitbucDelegate userJavabitbucDelegate;


    @RequestMapping(value = "/helloworld",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    public String helloworld( @RequestParam(value = "name", required = true) String name){

        return userJavabitbucDelegate.helloworld(name);
    }

}
